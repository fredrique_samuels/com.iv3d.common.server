package com.iv3d.common.server;

public class Response {
	private String status="ok";
	private String message="";
	private Object body=null;

	public String getStatus() {
		return status;
	}

	public Response setStatus(String status) {
		this.status = status;
		return this;
	}
	
	public Response setFailure(String message) {
		status="failed";
		setMessage(message);
		return this;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getBody() {
		return body;
	}

	public Response setBody(Object body) {
		this.body = body;
		return this;
	}
}
