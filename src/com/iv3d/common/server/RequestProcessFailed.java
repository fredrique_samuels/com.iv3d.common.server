package com.iv3d.common.server;

public class RequestProcessFailed extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public RequestProcessFailed(String msg) {
		super(msg);
	}
}
