package com.iv3d.common.server;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MultiPartRequest {

	private final InputStream inputStream;
	private final Map<String, String[]> params;

	public MultiPartRequest(InputStream inputStream, Map<String, String[]> params) {
		this.inputStream = inputStream;
		this.params = params;
	}

	public final InputStream getInputStream() {
		return inputStream;
	}
	
	public final String getParameter(String id) {
		if(params.containsKey(id)) {
			return params.get(id)[0];
		}
		return null;
	}
	
	public final Map<String,String[]> getParameterMap() {
		return new HashMap<String, String[]>(params);
	}
}
