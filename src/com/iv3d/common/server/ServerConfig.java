package com.iv3d.common.server;

import java.io.File;

import com.iv3d.common.utils.JsonUtils;

public class ServerConfig {

	private int port;
	private String dataDir;
	private long maxUploadSize;
	private int maxMemSize;
	private String tempUploadDir;

	public ServerConfig() {
	}

	public static ServerConfig load(String path) {
		return JsonUtils.readFromFile(new File(path), ServerConfig.class);
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDataDir() {
		return dataDir;
	}

	public void setDataDir(String dataDir) {
		this.dataDir = dataDir;
	}

	public long getMaxUploadSize() {
		return maxUploadSize;
	}

	public void setMaxUploadSize(long maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}

	public int getMaxMemSize() {
		return maxMemSize;
	}

	public void setMaxMemSize(int maxMemSize) {
		this.maxMemSize = maxMemSize;
	}

	public String getTempUploadDir() {
		return tempUploadDir;
	}

	public void setTempUploadDir(String tempUploadFolder) {
		this.tempUploadDir = tempUploadFolder;
	}

}
