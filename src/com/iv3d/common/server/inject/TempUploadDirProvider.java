package com.iv3d.common.server.inject;

import com.google.inject.Inject;
import com.iv3d.common.server.ServerConfig;

public final class TempUploadDirProvider {
	private final ServerConfig config;

	@Inject
	public TempUploadDirProvider(ServerConfigProvider config) {
		this.config = config.get();
	}

	public String get() {
		return config.getTempUploadDir();
	}
}
