package com.iv3d.common.server.inject;

import org.eclipse.jetty.server.handler.ResourceHandler;

public interface ResourceHandlerBuilder {
	ResourceHandler build();
}
