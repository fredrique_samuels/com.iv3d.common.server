package com.iv3d.common.server.inject;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.settings.ServerPortProvider;

@Singleton
public class ServerProvider {
	private final RequestPathResolverBuilder handlerFactory;
	private final ResourceHandlerBuilder resourceHandler;
	private ServerPortProvider port;
	private Server server;
	
	@Inject
	public ServerProvider(ServerPortProvider port,  
			RequestPathResolverBuilder handlerFactory,
			ResourceHandlerBuilder resourceHandler) {
		this.port = port;
		this.handlerFactory = handlerFactory;
		this.resourceHandler = resourceHandler;
	}

	public Server get(){
		if(server==null) {
			int i = port.get();
			server = new Server(i);
			
	        HandlerList handlers = new HandlerList();
	        handlers.setHandlers(new Handler[] { resourceHandler.build(), handlerFactory.build()});
	        
	        server.setHandler(handlers);
	        
		}
		return server;
	}
}
