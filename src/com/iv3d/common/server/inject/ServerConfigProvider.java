package com.iv3d.common.server.inject;

import java.io.File;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.iv3d.common.server.ServerConfig;
import com.iv3d.common.server.inject.markers.ServerConfigPathMarker;
import com.iv3d.common.utils.JsonUtils;

@Singleton
public final class ServerConfigProvider implements Provider<ServerConfig> {
	private final String path;
	private ServerConfig config;
	
	@Inject
	public ServerConfigProvider(@ServerConfigPathMarker String path) {
		this.path = path;
	}
	
	public ServerConfig get() {
		if(config==null) {
			config = JsonUtils.readFromFile(new File(path), ServerConfig.class);
		}
		return config;
	}
}
