package com.iv3d.common.server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.inject.Inject;
import com.iv3d.common.server.inject.TempUploadDirProvider;
import com.iv3d.common.server.inject.settings.MaxFileUploadSizeProvider;
import com.iv3d.common.server.inject.settings.MaxMemSizeProvider;

public class RequestMultiPartParser {

	private final long maxFileSize;
	private final int maxMemSize;
	private final String tempUploadDir;
	
	@Inject
	public RequestMultiPartParser(
			MaxFileUploadSizeProvider maxFileUploadSizeProvider,
			MaxMemSizeProvider maxMemSizeProvider,
			TempUploadDirProvider tempUploadDirProvider) {
		this.maxFileSize = maxFileUploadSizeProvider.get();
		this.maxMemSize = maxMemSizeProvider.get();
		this.tempUploadDir = tempUploadDirProvider.get();
	}
	
	public MultiPartRequest parse(HttpServletRequest request) throws IOException, FileUploadException {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			throw new  FileUploadException("Request is not a mutipart request!");
		}
	
		List<FileItem> fileItems = getFileItems(request);
		
		InputStream inputStream = null;
		Map<String, List<String>> paramList = new HashMap<String, List<String>>();
		
		Iterator<FileItem> i = fileItems.iterator();
		while (i.hasNext()) {
			FileItem fi = (FileItem) i.next();
			if (!fi.isFormField()) {
				inputStream = fi.getInputStream();
			} else {
				String fieldName = fi.getFieldName();
				if(!paramList.containsKey(fieldName)) {
					paramList.put(fieldName, new ArrayList<String>());
				}
				paramList.get(fieldName).add(fi.getString());
			}
		}
		

		Map<String, String[]> params = new HashMap<String, String[]>();
		for(String k:paramList.keySet()) {
			params.put(k, paramList.get(k).toArray(new String[]{}));
		}
		return new MultiPartRequest(inputStream, params);
	}

	@SuppressWarnings("unchecked")
	private List<FileItem> getFileItems(HttpServletRequest request) throws FileUploadException {
		ServletFileUpload upload = getFileItemExtractor();
		return upload.parseRequest(request);
	}

	private ServletFileUpload getFileItemExtractor() {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(maxMemSize);
		factory.setRepository(new File(tempUploadDir));
		
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(maxFileSize);
		return upload;
	}
	
	public class FileUploadError extends RuntimeException {
		public FileUploadError(FileUploadException e) {
			super(e);
		}

		private static final long serialVersionUID = 1L;

	}
}
