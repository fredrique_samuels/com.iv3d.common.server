package com.iv3d.common.server;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

public interface PathHandler {
	void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response);
	String getMethod();
}
